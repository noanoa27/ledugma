<?php

use yii\db\Migration;

/**
 * Class m180623_074029_init_rbac
 */
class m180623_074029_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $auth = Yii::$app->authManager;

        $teamleader = $auth->createRole('teamleader');
        $auth->add($teamleader);
  
        $admin = $auth->createRole('admin');
        $auth->add($admin);
                
        $teammember = $auth->createRole('teammember');
        $auth->add($teammember);

        $manager = $auth->createRole('manager');
        $auth->add($manager);
  
        $auth->addChild($admin, $teamleader);
        $auth->addChild($teamleader, $teammember);
        $auth->addChild($teammember, $manager);
  
        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);
  
        $updateBreakdown = $auth->createPermission('updateBreakdown');
        $auth->add($updateBreakdown);                    
                
       $viewOwnUser = $auth->createPermission('viewOwnUser');
  
        $createBreakdown = $auth->createPermission('createBreakdown');
        $auth->add($createBreakdown);  
        
        $changeLevel = $auth->createPermission('changeLevel');
        $auth->add($changeLevel); 

        $viewUser = $auth->createPermission('viewUser');
        $auth->add($viewUser); 
  
        $changeStatus = $auth->createPermission('changeStatus');
        $auth->add($changeStatus); 
  
        $rule = new \app\rbac\ViewRule; //לפי השם בתיקית rbac
      $auth->add($rule);
                
        $viewOwnUser->ruleName = $rule->name;                
        $auth->add($viewOwnUser);                 
                                  
                
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($teammember, $createBreakdown); 
         $auth->addChild($teammember, $updateBreakdown);
         $auth->addChild($teamleader, $changeLevel); 
         $auth->addChild($teamleader, $changeStatus);
         $auth->addChild($viewOwnUser, $viewUser);
         $auth->addChild($admin, $viewUser);
         $auth->addChild($teammember, $viewOwnUser);


        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180623_074029_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180623_074029_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
